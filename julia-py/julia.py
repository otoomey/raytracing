from typing import List
import math
import time
from complex import Complex

def findFilledJuliaSet(c: Complex, resolution: float = 0.01) -> List[Complex]:
    juliaSet = []

    angleSteps = math.ceil((2 * math.pi) / resolution) + 1
    modulusSteps = math.ceil(2 / resolution) + 1

    if Complex(0, 0).isWithinJuliaSet(c):
        juliaSet.append(Complex(0, 0))

    for angle in range(1, angleSteps):
        for modulus in range(modulusSteps):
            number = Complex.fromPolar(angle * resolution, modulus * resolution)
            if number.isWithinJuliaSet(c):
                juliaSet.append(number)

    return juliaSet

def findJuliaSet(c: Complex, resolution: float = 0.01) -> List[Complex]:
    juliaSet = []

    angleSteps = math.ceil((2 * math.pi) / resolution)
    modulusSteps = math.ceil(2 / resolution)

    if Complex(0, 0).isInJuliaSet(c):
        juliaSet.append(Complex(0, 0))

    for angle in range(1, angleSteps):
        for modulus in range(modulusSteps):
            number = Complex.fromPolar(angle * resolution, modulus * resolution)
            if number.isInJuliaSet(c):
                juliaSet.append(number)

    return juliaSet

# FIXME: This doesn't work
def printJuliaSet(c: Complex, resolution: float = 0.01):
    filename = f"./julia-set-{c}.txt"
    out = open(filename, "w")
    out.write(str(findJuliaSet(c, resolution)))
    print(f"Saved julia set for '{c}' as '{filename}'")

def printFilledJuliaSet(c: Complex, resolution: float = 0.01):
    filename = f"./julia-set-filled-{c}.txt"
    out = open(filename, "w")
    out.write(str(findFilledJuliaSet(c, resolution)))
    print(f"Saved filled julia set for '{c}' as '{filename}'")
