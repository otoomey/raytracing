from __future__ import annotations
import math

class Complex:
    @staticmethod
    def fromPolar(angle: float, modulus: float):
        real = modulus * math.cos(angle)
        imaginary = modulus * math.sin(angle)
        return Complex(real, imaginary)

    def __init__(self, real: float, imaginary: float):
        self.real = real
        self.imaginary = imaginary

    def __repr__(self):
        if self.imaginary < 0:
            return f"({str(self.real)}{str(self.imaginary)}i)"
        else:
            return f"({str(self.real)}+{str(self.imaginary)}i)"

    def __str__(self):
        return self.__repr__()

    def __getattr__(self, name):
        if name == "conjugate":
            return Complex(self.real, 0 - self.imaginary)
        elif name == "modulus":
            return (self.real**2 + self.imaginary**2)**(1/2)
        elif name == "polarAngle":
            # 1st quadrant
            if (self.real > 0) and (self.imaginary > 0):
                return math.atan(self.imaginary / self.real)
            # 2nd & 3rd quadrants
            elif (self.real < 0):
                return math.pi + math.atan(self.imaginary / self.real)
            # 4th quadrant
            elif (self.real > 0) and (self.imaginary < 0):
                return (2 * math.pi) + math.atan(self.imaginary / self.real)
            # Origin (no angle)
            else:
                return None
        else:
            raise AttributeError(f"Object 'Complex' has no attribute '{name}'")

    def __setattr__(self, name, value):
        readOnlyAttributes = ["conjugate", "modulus", "polarAngle"]

        if readOnlyAttributes.count(name) > 0:
            raise AttributeError(f"Cannot set read-only attribute '{name}' on object 'Complex'")
        else:
            return super().__setattr__(name, value)

    def __add__(self, other):
        if isinstance(other, Complex):
            return Complex(self.real + other.real, self.imaginary + other.imaginary)
        elif isinstance(other, int) or isinstance(other, float):
            return Complex(self.real + other, self.imaginary)
        else:
            return NotImplemented

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        if isinstance(other, Complex):
            return Complex(self.real - other.real, self.imaginary - other.imaginary)
        elif isinstance(other, int) or isinstance(other, float):
            return Complex(self.real - other, self.imaginary)
        else:
            return NotImplemented

    def __rsub__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return Complex(other - self.real, self.imaginary)
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Complex):
            real = (self.real * other.real) - (self.imaginary * other.imaginary)
            imaginary = (self.real * other.imaginary) + (self.imaginary * other.real)
            return Complex(real, imaginary)
        elif isinstance(other, int) or isinstance(other, float):
            return Complex(self.real * other, self.imaginary * other)
        else:
            return NotImplemented

    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        # if isinstance(other, ComplexNumber):
        #     real = (self.real * (1/other.real)) - (self.imaginary * (1/other.imaginary))
        #     imaginary = (self.real * (1/other.imaginary)) + (self.imaginary * (1/other.real))
        #     return ComplexNumber(real, imaginary)
        if isinstance(other, int) or isinstance(other, float):
            return Complex(self.real / other, self.imaginary / other)
        else:
            return NotImplemented

    def __pow__(self, other):
        result = self

        for i in range(1, other):
            result = result * self

        return result

    def isInJuliaSet(self, c: Complex, attempts: int = 250) -> bool:
        results = []
        current = self

        if c.modulus > 2:
            return False

        for i in range(attempts):
            current = (current**2) + c
            results.append(current.modulus)

            if current.modulus > 1:
                return False
            elif len(results) > 5 and (current.modulus < results[-2] < results[-3] < results[-4] < results[-5]):
                return False

        return True

    def isWithinJuliaSet(self, c: Complex, attempts: int = 250) -> bool:
        results = []
        current = self

        if c.modulus > 2:
            return False

        for i in range(attempts):
            current = (current**2) + c
            results.append(current.modulus)

            if current.modulus > 1:
                return False
            elif len(results) > 5 and (current.modulus < results[-2] < results[-3] < results[-4] < results[-5]):
                return True

        return True
