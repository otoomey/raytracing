from __future__ import annotations
import math

class Quaternion:
    def __init__(self, real: float = 0, i: float = 0, j: float = 0, k: float = 0):
        self.real = real
        self.i = i
        self.j = j
        self.k = k

    def __repr__(self):
        string = f"({self.real}"

        for symbol, part in {"i": self.i, "j": self.j, "k": self.k}.items():
            if (part >= 0):
                string += "+"

            string += f"{part}{symbol}"

        return f"{string})"

    def __str__(self):
        return self.__repr__()

    def __getattr__(self, name):
        if name == "conjugate":
            return Quaternion(self.real, 0 - self.i, 0 - self.j, 0 - self.k)
        elif name == "modulus":
            return (self.real**2 + self.i**2 + self.j**2 + self.k**2)**(1/2)
        else:
            raise AttributeError("Object 'Quaternion' has no attribute '" + name + "'")

    def __setattr__(self, name, value):
        readOnlyAttributes = ["conjugate", "modulus"]

        if readOnlyAttributes.count(name) > 0:
            raise AttributeError("Cannot set read-only attribute '" + name + "' on object 'Quaternion'")
        else:
            return super().__setattr__(name, value)

    def __add__(self, other):
        if isinstance(other, Quaternion):
            return Quaternion(self.real + other.real, self.i + other.i, self.j + other.j, self.k + other.k)
        elif isinstance(other, int) or isinstance(other, float):
            return Quaternion(self.real + other, self.i, self.j, self.k)
        else:
            return NotImplemented

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        if isinstance(other, Quaternion):
            return Quaternion(self.real - other.real, self.i - other.i, self.j - other.j, self.k - other.k)
        elif isinstance(other, int) or isinstance(other, float):
            return Quaternion(self.real - other, self.i, self.j, self.k)
        else:
            return NotImplemented

    def __rsub__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return Quaternion(other - self.real, self.i, self.j, self.k)
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Quaternion):
            # Multiplication table for quaternions, self is on the left, other is on the top
            # s/o | 1 |  i |  j |  k
            #  1  | 1 |  i |  j |  k
            #  i  | i | -1 |  k | -j
            #  j  | j | -k | -1 |  i
            #  k  | k |  j | -i | -1
            real = (self.real * other.real) - (self.i * other.i) - (self.j * other.j) - (self.k * other.k)
            i = (self.real * other.i) + (self.i * other.real) + (self.j * other.k) - (self.k * other.j)
            j = (self.real * other.j) + (self.j * other.real) + (self.k * other.i) - (self.i * other.k)
            k = (self.real * other.k) + (self.k * other.real) + (self.i * other.j) - (self.j * other.i)
            return Quaternion(real, i, j, k)
        elif isinstance(other, int) or isinstance(other, float):
            return Quaternion(self.real * other, self.i * other, self.j * other, self.k * other)
        else:
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            return self * other
        else:
            return NotImplemented

    def __truediv__(self, other):
        # Only added division by float (shrinking)
        if isinstance(other, int) or isinstance(other, float):
            return Quaternion(self.real / other, self.i / other, self.j / other, self.k / other)
        else:
            return NotImplemented

    def __pow__(self, other):
        # Relies wholly on the __mul__ function, just iterates multiplication
        result = self

        for i in range(1, other):
            result = result * self

        return result

    def isInJuliaSet(self, c: Quaternion, attempts: int = 250) -> bool:
        """Checks if the quaternion is in the julia set of another given quaternion, up to a certain amount of attempts"""
        results = []
        current = self

        if c.modulus > 2:
            return False

        for i in range(attempts):
            current = (current**2) + c
            results.append(current.modulus)

            if current.modulus > 1:
                return False
            elif len(results) > 5 and (current.modulus < results[-2] < results[-3] < results[-4] < results[-5]):
                return False

        return True

    def isWithinJuliaSet(self, c: Quaternion, attempts: int = 250) -> bool:
        """Checks if the quaternion is within the julia set of another given quaternion, up to a certain amount of attempts"""
        results = []
        current = self

        if c.modulus > 2:
            return False

        for i in range(attempts):
            current = (current**2) + c
            results.append(current.modulus)

            if current.modulus > 1:
                return False
            elif len(results) > 5 and (current.modulus < results[-2] < results[-3] < results[-4] < results[-5]):
                return True

        return True

    def isZero(self) -> bool:
        """Checks if all elements of the quaternion are zero (i.e. it is the origin)"""
        return self.real == 0 and self.i == 0 and self.j == 0 and self.k == 0
