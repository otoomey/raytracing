from typing import List
import math
import time
from quaternion import Quaternion

def findFilledJuliaSet(c: Quaternion, resolution: float = 0.01) -> List[Quaternion]:
    juliaSet = []

    steps = math.ceil(2 / resolution) + 1

    if Quaternion(0, 0, 0, 0).isWithinJuliaSet(c):
        juliaSet.append(Quaternion(0, 0, 0, 0))

    for r in range(-steps, steps):
        for i in range(-steps, steps):
            for j in range(-steps, steps):
                for k in range(-steps, steps):
                    number = Quaternion(r * resolution, i * resolution, j * resolution, k * resolution)

                    if number.isWithinJuliaSet(c):
                        juliaSet.append(number)

    return juliaSet

def findJuliaSet(c: Quaternion, resolution: float = 0.01) -> List[Quaternion]:
    """Computes the julia set for a given quaternion at the specified resolution (step size)"""
    juliaSet = []

    steps = math.ceil(2 / resolution) + 1

    for r in range(-steps, steps):
        for i in range(-steps, steps):
            for j in range(-steps, steps):
                for k in range(-steps, steps):
                    number = Quaternion(r * resolution, i * resolution, j * resolution, k * resolution)

                    if number.isInJuliaSet(c) and number.isZero() == False:
                        juliaSet.append(number)

    return juliaSet

def printJuliaSet(c: Quaternion, resolution: float = 0.01):
    """Outputs a text file with the julia set at a given quaternion and resolution in the format juliaQ-set-(quaternion)-r(resolution).txt"""
    filename = f"./juliaQ-set-{c}-r{resolution}.txt"
    out = open(filename, "w")

    for element in findJuliaSet(c, resolution):
        out.write(f"{element}\n")

    print(f"Saved julia quaternion set for '{c}' as '{filename}' @ steps of {resolution}")

def printFilledJuliaSet(c: Quaternion, resolution: float = 0.1):
    filename = f"./juliaQ-set-filled-{c}-r{resolution}.txt"
    out = open(filename, "w")

    for element in findFilledJuliaSet(c, resolution):
        out.write(f"{element}\n")

    print(f"Saved filled julia quaternion set for '{c}' as '{filename}' @ steps of {resolution}")

def closestPointTo(c: Quaternion, juliaSet: List[Quaternion]) -> Quaternion:
    """Find the closest quaternion in a set to a given quaternion"""
    closestDistance = distanceBetween(c, juliaSet[0])
    closestPoint = juliaSet[0]

    for point in juliaSet:
        distance = distanceBetween(c, point)

        if distance < closestDistance:
            closestDistance = distance
            closestPoint = point

    return closestPoint

def distanceBetween(a: Quaternion, b: Quaternion) -> float:
    """Calculate the distance between two quaternions"""
    # Square Root Of
    #   Sum Of
    #     Squares Of
    #       Distance between each part
    return math.sqrt((a.real - b.real)**2 + (a.i - b.i)**2 + (a.j - b.j)**2 + (a.k - b.k)**2)
