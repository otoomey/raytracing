﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayTracingMaster : MonoBehaviour
{

    struct Sphere
    {
        public Vector3 position;
        public float radius;
        public Vector3 albedo;
        public Vector3 specular;
        public float smoothness;
        public Vector3 emission;
    };

    public ComputeShader RayTracingShader;
    public Texture SkyboxTexture;
    public Light DirectionalLight;
    public Vector2 SphereRadius = new Vector2(3.0f, 8.0f);
    public uint SpheresMax = 100;
    public float SpherePlacementRadius = 100.0f;
    public int SphereSeed;

    private RenderTexture _target;
    private Camera _camera;
    private uint _currentSample = 0; // sample count determines how deep we are into the texture
    private Material _addMaterial; // a material that 'merges' per pixel ray values together so we get AA
    private ComputeBuffer _sphereBuffer; // buffer containing spheres in scene
    private RenderTexture _converged;

    private void OnEnable()
    {
        _currentSample = 0;
        SetupScene();
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            // camera position changed, need to reset sampling
            _currentSample = 0;
            transform.hasChanged = false;
        }
    }

    private void OnDisable() 
    {
        if (_sphereBuffer != null)
            _sphereBuffer.Release();
    }

    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        // Renders an image onto the camera
        SetShaderParameters();
        Render(destination);
    }


    /// <summary>
    /// Executes the compute shader onto the passed <paramref name="destination">texture</code>
    /// <param name="destination">A <see langword="RenderTexture"/> onto which to render. </param>
    /// </summary>
    private void Render(RenderTexture destination)
    {
        InitRenderTexture();

        RayTracingShader.SetTexture(0, "Result", _target);
        int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);
        RayTracingShader.Dispatch(0, threadGroupsX, threadGroupsY, 1);

        if (_addMaterial == null)
            _addMaterial = new Material(Shader.Find("Hidden/AddShader"));
        _addMaterial.SetFloat("_Sample", _currentSample);
        Graphics.Blit(_target, _converged, _addMaterial);
        Graphics.Blit(_converged, destination);
        _currentSample++;
    }

    /// <summary>
    /// Sends required shader parameters to compute shader
    /// </summary>
    private void SetShaderParameters()
    {
        // Camera transforms
        RayTracingShader.SetMatrix("_CameraToWorld", _camera.cameraToWorldMatrix);
        RayTracingShader.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);

        // Skybox
        RayTracingShader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);

        // AA
        RayTracingShader.SetVector("_PixelOffset", new Vector2(Random.value, Random.value));

        // Directional Light
        Vector3 l = DirectionalLight.transform.forward;
        RayTracingShader.SetVector("_DirectionalLight", new Vector4(l.x, l.y, l.z, DirectionalLight.intensity));

        // Scene spheres
        RayTracingShader.SetBuffer(0, "_Spheres", _sphereBuffer);

        // Random
        RayTracingShader.SetFloat("_Seed", Random.value);
    }

    /// <summary>
    /// Generates a bunch of spheres to put in the scene
    /// </summary>
    private void SetupScene()
    {
        List<Sphere> spheres = new List<Sphere>();

        Random.InitState(SphereSeed);

        // Add a number of random spheres
        for (int i = 0; i < SpheresMax; i++)
        {
            Sphere sphere = new Sphere();

            // Radius and radius
            sphere.radius = SphereRadius.x + Random.value * (SphereRadius.y - SphereRadius.x);
            Vector2 randomPos = Random.insideUnitCircle * SpherePlacementRadius;
            sphere.position = new Vector3(randomPos.x, sphere.radius, randomPos.y);

            // Reject spheres that are intersecting others
            foreach (Sphere other in spheres)
            {
                float minDist = sphere.radius + other.radius;
                if (Vector3.SqrMagnitude(sphere.position - other.position) < minDist * minDist)
                    goto SkipSphere;
            }

            // Albedo and specular color
            Color color = Random.ColorHSV();
            Color emission = Random.ColorHSV();

            bool metal = Random.value < 0.0f;
            bool emissive = Random.value < 0.1f;
            sphere.albedo = metal ? Vector3.zero : new Vector3(color.r, color.g, color.b);
            sphere.specular = metal ? new Vector3(color.r, color.g, color.b) : Vector3.one * 0.04f;
            sphere.smoothness = 1.0f;
            sphere.emission =  emissive ? new Vector3(emission.r, emission.g, emission.b) : Vector3.zero;

            // Add the sphere to the list
            spheres.Add(sphere);

        SkipSphere:
            continue;
        }

        // Assign to compute buffer
        _sphereBuffer = new ComputeBuffer(spheres.Count, 56);
        _sphereBuffer.SetData(spheres);
    }

    /// <summary>
    /// Initialises a new <see langref="RenderTexture"/> to <code>target</code> if the current one is null 
    /// or doesn't match screen size
    /// </summary>
    private void InitRenderTexture()
    {
        if (_target == null || _target.width != Screen.width || _target.height != Screen.height)
        {
            if (_target != null)
            {
                _target.Release();
            }

            _target = new RenderTexture(Screen.width, Screen.height, 0,
                    RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            _target.enableRandomWrite = true;
            _target.Create();

            _currentSample = 0;
        }
        if (_converged == null || _converged.width != Screen.width || _converged.height != Screen.height)
        {
            if (_converged != null)
            {
                _converged.Release();
            }

            _converged = new RenderTexture(Screen.width, Screen.height, 0,
                    RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            _converged.enableRandomWrite = true;
            _converged.Create();

            _currentSample = 0;
        }
    }
}
