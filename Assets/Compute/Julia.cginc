#include "Assets/Compute/Complex.cginc"
// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
#pragma exclude_renderers d3d11 gles

#ifndef RESOLUTION
#define RESOLUTION 0.01
#endif

#ifndef PI
#define PI 3.14159265f
#endif

#define JULIA_ATTEMPTS 250

bool IsInJuliaSet(Complex A, Complex B, uint attempts)
{
    if (B.modulus > 2) {
        return false;
    }

    Complex results[attempts];

    for (uint i = 0; i < attempts; i++)
    {
        A = add(pow(A, 2), B);
        results[i] = A.modulus;

        if (A.modulus > 1) 
        {
            return false;
        } 
        else if (i + 1 > 5 && 
            A.modulus < results[i-1] &&
            results[i-1] < results[i-2] &&
            results[i-3] < results[i-4])
        {
            return false;
        }
    }
    return true;
}

bool IsWithinJuliaSet(Complex A, Complex B, uint attempts)
{
    if (B.modulus > 2)
    {
        return false;
    }
    Complex results[attempts];
    for (uint i = 0; i < attempts; i++)
    {
        A = add(pow(A, 2), B);
        results[i] = A.modulus;

        if (A.modulus > 1)
        {
            return false;
        }
        else if (i + 1 > 5 && 
            A.modulus < results[i-1] &&
            results[i-1] < results[i-2] &&
            results[i-3] < results[i-4])
        {
            return true;
        }
    }
    return true;
}

Complex FindClosestInJuliaSet(Complex C, float3 position, float resolution)
{
    uint angleSteps = ceil((2 * PI) / RESOLUTION) + 1;
    uint modulusSteps = ceil(2 / RESOLUTION) + 1;

    Complex origin = CreateComplex(0, 0);
    Complex closest;
    if (IsWithinJuliaSet(origin, C, JULIA_ATTEMPTS))
    {
        closest = origin;
    }
    for (uint angle = 1; angle < angleSteps; angle++)
    {
        for (uint modulus = 0; modulus < modulusSteps; modulus++)
        {
            Complex number = FromPolar(angle * RESOLUTION, modulus * RESOLUTION);
            if (IsWithinJuliaSet(number, C, JULIA_ATTEMPTS) && distance())
            {
                
            }
        }
    }
    return closest;
}

