#ifndef COMPLEX_INCLUDED
#define COMPLEX_INCLUDED

struct Complex
{
    float real;
    float imaginary;
};

Complex CreateComplex(float real, float imaginary)
{
    Complex complex;
    complex.real = real;
    complex.imaginary = imaginary;
}

Complex FromPolar(float angle, float modulus)
{
    float real = modulus * cos(angle);
    float imaginary = modulus * sin(angle);
    return CreateComplex(real, imaginary);
}

Complex add(Complex A, Complex B)
{
    return CreateComplex(A.real + B.real, A.imaginary + B.imaginary);
}

Complex add(Complex A, float B)
{
    return CreateComplex(A.real + B, A.imaginary);
}

Complex sub(Complex A, Complex B)
{
    return CreateComplex(A.real - B.real, A.imaginary - B.imaginary);
}

Complex sub(Complex A, float B)
{
    return CreateComplex(A.real - B, A.imaginary);
}

Complex mul(Complex A, Complex B)
{
    float real = (A.real * B.real) - (A.imaginary * B.imaginary);
    float imaginary = (A.real * B.imaginary) + (A.imaginary * B.real);
    return CreateComplex(real, imaginary);
}

Complex mul(Complex A, float B)
{
    return CreateComplex(A.real * B, A.imaginary);
}

Complex div(Complex A, float B)
{
    return CreateComplex(A.real / B, A.imaginary / B);
}

Complex pow(Complex A, int B)
{
    Complex result = A;
    for (uint i = 0; i < B; i++)
    {
        result = mul(result, A);
    }
    return result;
}

#endif