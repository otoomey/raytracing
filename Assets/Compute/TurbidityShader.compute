﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

RWTexture2D<float4> _Input;
RWTexture2D<float4> Result;
float2 _Resolution;

#define SAMPLES 10

[numthreads(8,8,1)]
void CSMain (uint3 id : SV_DispatchThreadID)
{
    int marchDist = floor(SAMPLES / 2.0f);
    float weightedTotal = 0.0f;
    float weightTotal = 0.0f;
    float maxDist = sqrt(pow(marchDist, 2)*2); 
    for (int x = max(0, id.x-marchDist); x < min(_Resolution.x, id.x + marchDist); x++)
    {
        for (int y = max(0, id.y-marchDist); y < min(_Resolution.y, id.y + marchDist); y++)
        {
            float dist = distance(float2(x, y), float2(id.xy));
            float weight = 1 - (dist / maxDist);
            weightedTotal += weight * _Input[uint2(x,y)];
            weightTotal += weight;
        }
    }
    Result[id.xy] = float4(1.0f, 1.0f, 1.0f, 1.0f) * abs(_Input[id.xy] - (weightedTotal / weightTotal));
}
