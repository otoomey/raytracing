﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurbidityShaderMaster : MonoBehaviour
{
    public ComputeShader TurbidityShader;
    
    public void computeTurbidityMap(RenderTexture _source, RenderTexture _target, bool useTargetResolution = false) {
        Vector2 resolution = useTargetResolution 
                ? new Vector2(_target.width, _target.height) 
                : new Vector2(_source.width, _source.height);
        TurbidityShader.SetTexture(0, "_Input", _source);
        TurbidityShader.SetTexture(0, "Result", _target);
        TurbidityShader.SetVector("_Resolution", resolution);
        int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);
        TurbidityShader.Dispatch(0, threadGroupsX, threadGroupsY, 1);
    }
}
