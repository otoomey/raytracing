﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayMarchingMaster : MonoBehaviour
{
    public ComputeShader RayMarchingShader;
    public Texture SkyboxTexture;

    private RenderTexture _target;
    private RenderTexture _converged;
    private Camera _camera;
    private uint _currentSample = 0; // sample count determines how deep we are into the texture
    private Material _addMaterial; // a material that 'merges' per pixel ray values together so we get AA
    
    private void OnEnable()
    {
        _currentSample = 0;
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            // camera position changed, need to reset sampling
            _currentSample = 0;
            transform.hasChanged = false;
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        SetShaderParameters();
        Render(dest);
    }

    void SetShaderParameters()
    {
        RayMarchingShader.SetMatrix("_CameraToWorld", _camera.cameraToWorldMatrix);
        RayMarchingShader.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);
        RayMarchingShader.SetVector("_PixelOffset", new Vector2(Random.value, Random.value));

        RayMarchingShader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);
        RayMarchingShader.SetFloat("_Seed", Random.value);
    }

    private void Render(RenderTexture destination)
    {
        InitRenderTexture();
        RayMarchingShader.SetTexture(0, "Result", _target);
        int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);
        RayMarchingShader.Dispatch(0, threadGroupsX, threadGroupsY, 1);

        if (_addMaterial == null)
            _addMaterial = new Material(Shader.Find("Hidden/AddShader"));
        _addMaterial.SetFloat("_Sample", _currentSample);
        
        Graphics.Blit(_target, _converged, _addMaterial);
        Graphics.Blit(_converged, destination);
        _currentSample++;
    }

    private void InitRenderTexture()
    {
        if (_target == null || _target.width != Screen.width || _target.height != Screen.height)
        {
            if (_target != null)
                _target.Release();

            if (_converged != null)
                _converged.Release();

            _target = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            _target.enableRandomWrite = true;
            _target.Create();

            _converged = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            _converged.enableRandomWrite = true;
            _converged.Create();

            _currentSample = 0;
        }
    }
}
