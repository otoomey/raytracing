﻿Shader "Hidden/FireflyShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f input) : SV_Target
            {
                float4 col = tex2D(_MainTex, input.uv);
                float4 avgCol = 0;
                float invAspect = _ScreenParams.y / _ScreenParams.x;
                int samples = 10.0;

                for (uint i = 0; i < samples; i++)
                {
                    float offsetX = (i / (samples-1) - 0.5f) * 0.01f * invAspect;
                    for (uint j = 0; j < samples; j++)
                    {
                        float offsetY = (j / (samples-1) - 0.5f) * 0.01f * invAspect;
                        float2 uv = input.uv + float2(offsetX, offsetY);
                        avgCol += tex2D(_MainTex, uv);
                    }
                }
                
                avgCol /= (samples * samples);
                if (length(col) > length(avgCol) * 1.5) {
                    return avgCol;
                }
                return col;
            }
            ENDCG
        }
    }
}
